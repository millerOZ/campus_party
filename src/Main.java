package campus_party.src;

import javax.swing.JOptionPane;

public class Main {

    public static void main(String arg[]) {
        Metodo metodo = new Metodo();
        boolean bandera = true;
        while (bandera) {
            int opcion = Integer.parseInt(JOptionPane.showInputDialog(null,
                      "1. Almacenar campusero. \n"
                    + "2. Almacenar pabellon. \n"
                    + "3. Almacenar ciudad. \n"
                    + "4. Mostrar campusero por pabellon. \n"
                    + "5. Mostrar todos los campuseros. \n"
                    + "6. Mostrar los pabellones. \n"
                    + "7. Mostrar los parqueadero. \n"
                    + "8. Mostrar las ciudades. \n"
                    + "9. Salir"
            ));
            switch (opcion) {
                case 1:
                    metodo.ingresarCampusero();
                    break;
                case 2:
                    metodo.ingresarPabellon();
                    break;
                case 3:
                    metodo.ingresarCiudad();
                  
                    break;
                case 9:
                    bandera = false;
                    break;
                default:
                    JOptionPane.showInputDialog(null, "opción incorrecta !!");
                    break;
            }
        }
    }
}
