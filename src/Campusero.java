package numeroPrimo;

public class Campusero {
    boolean tieneCarpa;
    String telefono, email,cedula,nombre,ciudadOrigen;

    public Campusero(boolean tieneCarpa, String telefono, String email, String cedula, String nombre, String ciudadOrigen) {
        this.tieneCarpa = tieneCarpa;
        this.telefono = telefono;
        this.email = email;
        this.cedula = cedula;
        this.nombre = nombre;
        this.ciudadOrigen = ciudadOrigen;
    }

    public boolean isTieneCarpa() {
        return tieneCarpa;
    }

    public void setTieneCarpa(boolean tieneCarpa) {
        this.tieneCarpa = tieneCarpa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudadOrigen() {
        return ciudadOrigen;
    }

    public void setCiudadOrigen(String ciudadOrigen) {
        this.ciudadOrigen = ciudadOrigen;
    }
    public void EstoesUnaPrueba(){
      /*
    el proyecto final LLLLLLLLLLLL
      */
    }

}
